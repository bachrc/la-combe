# Coffee Machine 

This repo contains the La Combe du Lion Vert technical test, 
which has been done in less than two hours.

## Launch instructions

You can checkout commits in order to rewind to iterations. You can then test with :

```
mvn clean test
```