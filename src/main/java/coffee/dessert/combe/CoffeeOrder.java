package coffee.dessert.combe;

import coffee.dessert.combe.exceptions.CoffeeException;

/**
 * Class representing a coffee order
 */
public class CoffeeOrder implements Instruction {

    private Drink drinkType;
    private int sugars;
    private boolean stick;
    private boolean extraHot;

    private double givenMoney;

    public CoffeeOrder(Drink drinkType, int sugars, boolean stick) {
        this(drinkType, sugars, stick, 0.0);
    }

    public CoffeeOrder(Drink drinkType, int sugars, boolean stick, double givenMoney) {
        this(drinkType, sugars, stick, givenMoney, false);
    }

    public CoffeeOrder(Drink drinkType, int sugars, boolean stick, double givenMoney, boolean extraHot) {
        this.setDrinkType(drinkType);
        this.setSugars(sugars);
        this.setStick(stick);

        this.setGivenMoney(givenMoney);
        this.setExtraHot(extraHot);

    }

    public Drink getDrinkType() {
        return drinkType;
    }

    public CoffeeOrder setDrinkType(Drink drinkType) {
        this.drinkType = drinkType;

        return this;
    }

    public int getSugars() {
        return sugars;
    }

    public CoffeeOrder setSugars(int sugars) {
        if(sugars < 0 || sugars > 2) {
            throw new CoffeeException("Invalid number of sugars");
        }

        this.sugars = sugars;

        return this;
    }

    public boolean isStick() {
        return stick;
    }

    public CoffeeOrder setStick(boolean stick) {
        this.stick = stick;

        return this;
    }

    public double getGivenMoney() {
        return givenMoney;
    }

    public CoffeeOrder setGivenMoney(double givenMoney) {
        this.givenMoney = givenMoney;

        return this;
    }

    public boolean isExtraHot() {
        return extraHot;
    }

    public void setExtraHot(boolean extraHot) {
        if (!this.drinkType.isExtraHotPossibility() && extraHot) {
            throw new CoffeeException("This drink cannot be extra hot");
        }

        this.extraHot = extraHot;
    }

    public String getRepresentation() {
        if (drinkType.getPrice() > getGivenMoney()) {
            return new Message(String.format("Not enough money was given : you need %.2f€ more",
                    Math.abs(drinkType.getPrice() - getGivenMoney()))).getRepresentation();
        }

        StringBuilder sb = new StringBuilder();

        sb.append(this.drinkType.getSymbol());

        if (this.extraHot) {
            sb.append("h");
        }

        sb.append(":");

        if(this.sugars > 0) {
            sb.append(this.sugars);
        }

        sb.append(":");

        if(this.stick) {
            sb.append("0");
        }

        return sb.toString();
    }
}
