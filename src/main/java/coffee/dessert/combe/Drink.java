package coffee.dessert.combe;

public enum Drink {
    COFFEE("C", 0.6, true),
    CHOCOLATE("H", 0.5, true),
    TEA("T", 0.4, true),
    ORANGE_JUICE("O", 0.6, false);

    private String symbol;
    private double price;
    private boolean extraHotPossibility;

    Drink(String symbol, double price, boolean extraHotPossibility) {
        this.symbol = symbol;
        this.price = price;

        this.extraHotPossibility = extraHotPossibility;
    }

    public String getSymbol() {
        return symbol;
    }

    public double getPrice() {
        return price;
    }

    public boolean isExtraHotPossibility() {
        return extraHotPossibility;
    }
}
