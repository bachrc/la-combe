package coffee.dessert.combe;

public interface Instruction {
    public String getRepresentation();
}
