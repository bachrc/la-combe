package coffee.dessert.combe;

public class Message implements Instruction {
    private String message;

    public Message(String message) {
        setMessage(message);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRepresentation() {
        return String.format("M:%s", this.message);
    }
}
