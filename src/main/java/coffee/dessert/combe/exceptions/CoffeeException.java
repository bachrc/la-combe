package coffee.dessert.combe.exceptions;

public class CoffeeException extends RuntimeException {
    private String message;

    public CoffeeException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return String.format("Coffee exception : %s", this.message);
    }
}
