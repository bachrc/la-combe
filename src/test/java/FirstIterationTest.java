import coffee.dessert.combe.CoffeeOrder;
import coffee.dessert.combe.Drink;
import coffee.dessert.combe.Message;
import coffee.dessert.combe.exceptions.CoffeeException;
import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.fail;

public class FirstIterationTest {
    @Test
    public void shouldOutputProtocolCorrectly() {
        CoffeeOrder coffee1 = new CoffeeOrder(Drink.CHOCOLATE, 1, true, 1);
        CoffeeOrder coffee2 = new CoffeeOrder(Drink.TEA, 0, true, 1);
        CoffeeOrder coffee3 = new CoffeeOrder(Drink.COFFEE, 1, false, 1);

        Assert.assertTrue(coffee1.isStick());
        Assert.assertEquals(Drink.COFFEE, coffee3.getDrinkType());
        Assert.assertEquals(0, coffee2.getSugars());

        Assert.assertEquals("H:1:0", coffee1.getRepresentation());
        Assert.assertEquals("T::0", coffee2.getRepresentation());
        Assert.assertEquals("C:1:", coffee3.getRepresentation());
    }

    @Test
    public void shouldThrowIfInvalidSugars() {
        try {
            new CoffeeOrder(Drink.COFFEE, 3, true, 1);
            fail();
        } catch(CoffeeException e) {
            Assert.assertEquals("Coffee exception : Invalid number of sugars", e.getMessage());
        }
    }

    @Test
    public void testMessages() {
        String message = "Hello Alchemist, the end is near";

        Message mess = new Message(message);

        Assert.assertEquals(message, mess.getMessage());
        Assert.assertEquals(String.format("M:%s", message), mess.getRepresentation());
    }
}
