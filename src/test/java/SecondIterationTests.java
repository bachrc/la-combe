
import coffee.dessert.combe.CoffeeOrder;
import coffee.dessert.combe.Drink;
import org.junit.Assert;
import org.junit.Test;

public class SecondIterationTests {
    @Test
    public void shouldNotAcceptLessMoney() {
        CoffeeOrder coffee = new CoffeeOrder(Drink.COFFEE, 1, false, 0.2);
        CoffeeOrder coffee2 = new CoffeeOrder(Drink.COFFEE, 1, true);

        Assert.assertEquals("M:Not enough money was given : you need 0,40€ more", coffee.getRepresentation());
        Assert.assertEquals("M:Not enough money was given : you need 0,60€ more", coffee2.getRepresentation());
    }
}
