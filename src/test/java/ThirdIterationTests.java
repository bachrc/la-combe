import coffee.dessert.combe.CoffeeOrder;
import coffee.dessert.combe.Drink;
import coffee.dessert.combe.exceptions.CoffeeException;
import org.junit.Assert;
import org.junit.Test;

public class ThirdIterationTests {
    @Test
    public void shouldFormatProtocolCorrectly() {
        CoffeeOrder chocolate = new CoffeeOrder(Drink.CHOCOLATE, 0, false, 0.5, true);

        Assert.assertTrue(chocolate.isExtraHot());
        Assert.assertEquals("Hh::", chocolate.getRepresentation());
    }

    @Test(expected = CoffeeException.class)
    public void shouldNotAcceptWarmOrangeJuice() {
        new CoffeeOrder(Drink.ORANGE_JUICE, 2, true, 0.7, true);
    }


}
